package goresolve

import (
	"io/ioutil"
	"log"
	"net"
	"strconv"
	"sync"
	"time"

	"github.com/miekg/dns"
)

const (
	DNSPort = 53

	GoogleDNS     = "8.8.8.8"
	CloudflareDNS = "1.1.1.1"

	DefaultTimeout = time.Second
	DefaultNetwork = "udp"
)

var discardLogger = log.New(ioutil.Discard, "", 0)

type Resolver struct {
	lock sync.Mutex

	ServerAddress string
	Network       string
	Timeout       time.Duration
	Logger        *log.Logger

	conn        *dns.Conn
	sentQueries map[uint16]*dnsQuery

	id uint16

	running bool
	done    chan bool
}

// Start sets default values for unset fields and starts service goroutines
func (r *Resolver) Start() error {
	r.lock.Lock()
	defer r.lock.Unlock()

	if r.running {
		return nil
	}

	r.setDefaults()

	var conn, err = dns.DialTimeout(r.Network, net.JoinHostPort(r.ServerAddress, strconv.Itoa(DNSPort)), r.Timeout)
	if err != nil {
		return err
	}

	r.conn = conn
	r.sentQueries = make(map[uint16]*dnsQuery)
	r.running = true
	r.done = make(chan bool, 2)
	dns.Id = func() uint16 {
		var id = r.id
		r.id += 1
		return id
	}

	go func() {
		var t = r.Timeout/3 + time.Millisecond // time magic
		var running = true
		for running {
			time.Sleep(t)
			running = r.checkTimeouts()
		}
		r.done <- true
	}()

	go func() {
		var running = true
		for running {
			running = r.receiveMessage()
		}
		r.done <- true
	}()

	return nil
}

// LookupIP asynchronously sends a DNS question for type A resource records and returns a promise
func (r *Resolver) LookupIP(host string) *QueryResult {
	r.lock.Lock()
	defer r.lock.Unlock()

	if !r.running {
		return makeEmptyResult()
	}

	return r.openQuery(host)
}

// Stop stops the resolver's service goroutines and discards all pending queries
func (r *Resolver) Stop() {
	r.lock.Lock()
	defer r.lock.Unlock()

	r.stop()
}

// Block the calling goroutine until all of resolver's service goroutines stop
func (r *Resolver) WaitCleanup() {
	<-r.done // whoa technology
	<-r.done
}

func (r *Resolver) setDefaults() {
	if r.ServerAddress == "" {
		r.ServerAddress = GoogleDNS
	}
	if r.Network == "" {
		r.Network = DefaultNetwork
	}
	if r.Timeout == 0 {
		r.Timeout = DefaultTimeout
	}
	if r.Logger == nil {
		r.Logger = discardLogger
	}
}

func (r *Resolver) checkTimeouts() bool {
	r.lock.Lock()
	defer r.lock.Unlock()

	if !r.running {
		return false
	}

	for _, query := range r.sentQueries {
		if time.Since(query.sendTime) > r.Timeout {
			if query.retriesLeft() {
				if err := query.sendTo(r.conn); err != nil {
					r.Logger.Println("resend query:", err)
					r.stop()
					return false
				}
			} else {
				r.closeQuery(query, nil, NoRetriesLeftError)
			}

		}
	}

	return r.running
}

func (r *Resolver) receiveMessage() bool {
	var m, err = r.conn.ReadMsg()

	r.lock.Lock()
	defer r.lock.Unlock()

	if !r.running {
		return false
	}

	if err != nil {
		r.Logger.Println("receive message:", err)
		r.stop()
		return false
	}

	var id = m.Id
	var query, present = r.sentQueries[id]
	if present {
		var ips = make([]net.IP, 0, len(m.Answer))

		for _, rr := range m.Answer {
			if a, ok := rr.(*dns.A); ok {
				ips = append(ips, a.A)
			}
		}

		if len(ips) == 0 {
			r.closeQuery(query, nil, NoRecordError)
		} else {
			r.closeQuery(query, ips, nil)
		}
	} else {
		r.Logger.Println("no query pending for Id", id)
	}

	return r.running
}

func (r *Resolver) openQuery(host string) *QueryResult {
	var query, result = newQuery(host)

	go func() {
		r.lock.Lock()
		defer r.lock.Unlock()

		if err := query.sendTo(r.conn); err != nil {
			r.Logger.Println("send query:", err)
			query.discard(err)
			r.stop()
			return
		}

		if _, present := r.sentQueries[query.message.Id]; present {
			r.Logger.Println("duplicate message Id found")
			query.discard(TooManyQueries)
			return
		}
		r.sentQueries[query.message.Id] = query
	}()

	return result
}

func (r *Resolver) closeQuery(q *dnsQuery, ips []net.IP, err error) {
	if err == nil {
		q.finish(ips)
	} else {
		q.discard(err)
		r.Logger.Printf("discarded query for \"%s\": %v\n", q.message.Question[0].Name, err)
	}
	delete(r.sentQueries, q.message.Id)
}

func (r *Resolver) stop() {
	if !r.running {
		return
	}

	r.running = false
	_ = r.conn.Close()

	for _, query := range r.sentQueries {
		r.Logger.Println("resolver stop: discarded", query.message.Id)
		r.closeQuery(query, nil, ResolverNotRunningError)
	}
}
