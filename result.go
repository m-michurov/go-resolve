package goresolve

import (
	"net"
)

type QueryResult struct {
	ips    <-chan []net.IP
	errors <-chan error
}

// Await blocks the calling goroutine until query results become available
func (r *QueryResult) Await() ([]net.IP, error) {
	return <-r.ips, <-r.errors
}

func makeEmptyResult() *QueryResult {
	var answers = make(chan []net.IP, 1)
	var errors = make(chan error, 1)

	errors <- ResolverNotRunningError
	answers <- nil
	close(errors)
	close(answers)

	return &QueryResult{
		ips:    answers,
		errors: errors,
	}
}
