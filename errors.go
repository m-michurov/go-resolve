package goresolve

type ResolveError string

func (r ResolveError) Error() string {
	return string(r)
}

const (
	// NoRecordError indicates that no resource records were returned by server
	NoRecordError ResolveError = "server returned no records"

	// NoRetriesLeftError indicates that server did not answer a DNS query
	NoRetriesLeftError ResolveError = "no retries left"

	// ResolverNotRunningError is returned by LookupIP when invoked on a resolver that is not running
	ResolverNotRunningError ResolveError = "resolver is not running"

	TooManyQueries ResolveError = "too many unanswered requests"
)
