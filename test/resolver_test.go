package test

import (
	"fmt"
	"log"
	"os"
	"sync"
	"testing"
	"time"

	dns "gitlab.com/m-michurov/go-resolve"
)

func panicOnTimeout(t time.Duration) {
	<-time.After(t)
	panic("timeout exceeded")
}

func TestLookup(t *testing.T) {
	const N, NTest = 100, 10
	const timeout = 5 * time.Second
	time.Sleep(timeout)
	go panicOnTimeout(timeout)

	for i := 0; i < NTest; i += 1 {
		doRequests(t, N)
	}
}

func doRequests(t *testing.T, N int) {
	var resolver = dns.Resolver{ServerAddress: dns.CloudflareDNS, Logger: log.New(os.Stdout, "", log.Flags())}
	if err := resolver.Start(); err != nil {
		t.Fatal("start resolver:", err)
	}

	var hosts = []string{"www.google.com", "www.youtube.com", "vk.com"}

	var wg = sync.WaitGroup{}
	wg.Add(N * len(hosts))

	for _, host := range hosts {
		host_ := host
		for i := 0; i < N; i += 1 {
			var result = resolver.LookupIP(host_)
			go func() {
				defer wg.Done()
				var _, err = result.Await()
				if err != nil {
					t.Errorf("host name \"%s\" not resolved\n", host_)
				}
			}()
		}
	}

	wg.Wait()
	resolver.Stop()
	resolver.WaitCleanup()
}

func TestLookupPrint(t *testing.T) {
	var resolver = dns.Resolver{}
	if err := resolver.Start(); err != nil {
		t.Fatal("start resolver:", err)
	}

	var hosts = []string{"www.google.com", "www.youtube.com", "vk.com", "www.vk.com", "www.vk.com."}

	var wg = sync.WaitGroup{}
	wg.Add(len(hosts))

	for _, host := range hosts {
		host_ := host
		var result = resolver.LookupIP(host_)
		go func() {
			defer wg.Done()
			var ips, err = result.Await()
			if err != nil {
				t.Error(err)
			}
			fmt.Printf("%s -> %v\n", host_, ips)
		}()
	}

	wg.Wait()
	resolver.Stop()
	resolver.WaitCleanup()
}

func TestNotRunningResolver(t *testing.T) {
	var r = dns.Resolver{}
	var promise = r.LookupIP("google.com")
	var _, err = promise.Await()
	if err == nil {
		t.Fatalf("no error")
	}
}
