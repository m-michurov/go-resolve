package goresolve

import (
	"github.com/miekg/dns"
	"net"
	"strings"
	"time"
)

const maxRetries = 5

type dnsQuery struct {
	message  *dns.Msg
	retries  int
	sendTime time.Time
	answers  chan<- []net.IP
	errors   chan<- error
}

func newQuery(host string) (*dnsQuery, *QueryResult) {
	if !strings.HasSuffix(host, ".") {
		host += "."
	}
	var m = &dns.Msg{}
	m.SetQuestion(host, dns.TypeA)

	var answers = make(chan []net.IP, 1)
	var errors = make(chan error, 1)

	return &dnsQuery{
			message: m,
			retries: 0,
			answers: answers,
			errors:  errors,
		}, &QueryResult{
			ips:    answers,
			errors: errors,
		}
}

func (q dnsQuery) retriesLeft() bool {
	return q.retries < maxRetries
}

func (q *dnsQuery) sendTo(conn *dns.Conn) error {
	if err := conn.WriteMsg(q.message); err != nil {
		return err
	}
	q.retries += 1
	q.sendTime = time.Now()

	return nil
}

func (q dnsQuery) discard(why error) {
	q.errors <- why
	q.answers <- nil

	q.close()
}

func (q dnsQuery) finish(ips []net.IP) {
	q.errors <- nil
	q.answers <- ips

	q.close()
}

func (q dnsQuery) close() {
	close(q.errors)
	close(q.answers)
}
